## SOS Game

This is a (very) simple Tic Tac Toe modified game called SOS. This game inspired from Hago (mobile multiplayer game platform), instead using XXX or OOO, this game use SOS, consist of 2 players.  
  
This game fully created by using ReactJS.  
For now, it only support PC / Computer display, not yet supported for mobile display. This feature will come soon.  
This just my personal project to fullfil my hobby and to do something productive during winter holiday, so I am sorry if this app will not be as good as you expected. 

To run this app, you have to install NodeJS First, and run those command in the same directory as the files:

1. **npm i**  
   This will install all dependencies required to run this game
2. **npm start**  
   This will open your browser and start the game
   
And see the result in your browser at address http://localhost:3000

[SEE MY APP HERE](https://sos-game.herokuapp.com)

Created by: Pradipta Gitaya