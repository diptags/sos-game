import React, { Component } from 'react';

class Square extends Component{
	render(){
    let value = null;
    if(this.props.value === 'S') value = <p className="text-primary">{this.props.value}</p>
    else value = <p className="text-danger">{this.props.value}</p>
		return(
			<button
				className="square"
				onClick={() => this.props.click()}
			>
			{value}
			</button>
		);			
	}
}
export default Square;